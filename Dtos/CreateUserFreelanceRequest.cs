﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace NexoProfessional.Dtos
{
    public class CreateUserFreelanceRequest
    {
        [Required]
        [MaxLength(200)]
        public string Name { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string Alias { get; set; } = string.Empty;

        [Required]
        [MaxLength(320)]
        public string Email { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string SurName { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string Dni { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string Telephone { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string CountryShort { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string CountryLong { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string Province { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string City { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string Availability { get; set; } = string.Empty;

        [Required]
        public bool IsFreelance { get; set; }

        [Required]
        [MaxLength(200)]
        [NotMapped]
        public string[] Activities { get; set; } = Array.Empty<string>();

        [Required]
        [MaxLength(200)]
        [NotMapped]
        public string[] Skills { get; set; } = Array.Empty<string>();

        [Required]
        [MaxLength(200)]
        public string Proyect { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string Description { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string Tarifa { get; set; } = string.Empty;

        [Required]
        public bool isNexo { get; set; }

        [Required]
        [MaxLength(200)]
        public string imgProfile { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string Type { get; set; } = string.Empty; // Freelance o Client

        // Role
        public bool isAdmin { get; set; } = false;

        // Usuario verificado
        [Column("isConfirm")]
        public bool isConfirm { get; set; } = false;
    }
}
