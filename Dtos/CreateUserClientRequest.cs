﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NexoProfessional.Dtos
{
    public class CreateUserClientRequest
    {
        [Required]
        [MaxLength(200)]
        public string Name { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string SurName1 { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string SurName2 { get; set; } = string.Empty;

        [Required]
        [MaxLength(320)]
        public string Email { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string Telephone { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string CompanyName { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string CIF { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string CompanyTelephone { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string CompanyEmail { get; set; } = string.Empty;

        [Required]
        [MaxLength(400)]
        public string Password { get; set; } = string.Empty;

        [Required]
        [MaxLength(400)]
        public string ConfirmPassword { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string CountryShort { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string CountryLong { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string Province { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string City { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string Proyect { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        public string Type { get; set; } = string.Empty; // Freelance o Client

        // Role
        public bool isAdmin { get; set; } = false;

        // Usuario verificado
        [Column("isConfirm")]
        public bool isConfirm { get; set; } = false;
    }
}
