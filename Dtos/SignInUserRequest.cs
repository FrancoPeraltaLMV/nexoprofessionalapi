﻿using System.ComponentModel.DataAnnotations;

namespace NexoProfessional.Dtos
{
    public class SignInUserRequest
    {
        [Required]
        [MaxLength(320)]
        public string Email { get; set; } = string.Empty;

        [Required]
        [MaxLength(64)]
        public string Password { get; set; } = string.Empty;
    }
}
