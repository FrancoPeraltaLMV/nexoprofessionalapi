﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NexoProfessional.Migrations
{
    /// <inheritdoc />
    public partial class AddIsConfirmUsers : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isConfirm",
                table: "Users",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isConfirm",
                table: "Users");
        }
    }
}
