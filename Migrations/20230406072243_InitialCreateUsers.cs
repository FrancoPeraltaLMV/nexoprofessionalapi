﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NexoProfessional.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreateUsers : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropTable(
                name: "Freelances");

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    alias = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    surname = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    dni = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    surname1 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    surname2 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    email = table.Column<string>(type: "nvarchar(320)", maxLength: 320, nullable: false),
                    password = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: false),
                    confirmpassword = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: false),
                    telephone = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    companyname = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    cif = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    companytelephone = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    companyemail = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    countryshort = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    countrylong = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    province = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    city = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    availability = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    isfreelance = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    proyect = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    description = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    tarifa = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    isnexo = table.Column<bool>(type: "bit", nullable: false),
                    type = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    imgprofile = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    isAdmin = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BajaLogica = table.Column<bool>(type: "bit", nullable: false),
                    cif = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    companyemail = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    companyname = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    companytelephone = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    country = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    email = table.Column<string>(type: "nvarchar(320)", maxLength: 320, nullable: false),
                    location = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    province = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    proyect = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    surname1 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    surname2 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    telephone = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Freelances",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    alias = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    availability = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    BajaLogica = table.Column<bool>(type: "bit", nullable: false),
                    country = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    description = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    dni = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    email = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    isfreelance = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    password = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    proyect = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    surname = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    tarifa = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    telephone = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Freelances", x => x.id);
                });
        }
    }
}
