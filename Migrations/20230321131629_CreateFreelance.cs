﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NexoProfessional.Migrations
{
    /// <inheritdoc />
    public partial class CreateFreelance : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Freelances",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    surname = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    alias = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    dni = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    email = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    telephone = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    country = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    availability = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    isfreelance = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    proyect = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    description = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    tarifa = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    password = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    BajaLogica = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Freelances", x => x.id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Freelances");
        }
    }
}
