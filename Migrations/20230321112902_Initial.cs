﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NexoProfessional.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    email = table.Column<string>(type: "nvarchar(320)", maxLength: 320, nullable: false),
                    cif = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    companyemail = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    companyname = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    companytelephone = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    country = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    province = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    location = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    proyect = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    surname1 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    surname2 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    telephone = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    BajaLogica = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Clients");
        }
    }
}
