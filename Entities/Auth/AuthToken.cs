﻿namespace NexoProfessional.Entities.Auth
{
    public class AuthToken
    {
        public string AccessToken { get; set; } = string.Empty;

        public double Expires { get; set; } = double.NaN;
    }
}
