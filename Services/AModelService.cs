﻿using NexoProfessional.Repository;

namespace NexoProfessional.Services
{
    public abstract class AModelService<T> : IModelService<T> where T : class
    {
        protected readonly IModelRepository<T> _modelRepository;

        public AModelService(IModelRepository<T> modelRepository)
        {
            _modelRepository = modelRepository;
        }

        public async Task<T> Add(T model)
        {
            return await _modelRepository.CreateAsync(model);
        }

        public async Task<bool> Update(int id, T newModel)
        {
            var oldModel = await _modelRepository.GetByIdAsync(id);

            if (oldModel == null)
            {
                return false;
            }

            this.SetUpdateData(oldModel, newModel);

            await _modelRepository.UpdateAsync(oldModel);

            return true;

        }

        protected abstract void SetUpdateData(T oldModel, T newModel);


        public async Task<bool> Delete(int id)
        {
            await _modelRepository.DeleteAsync(id);

            return true;
        }

        public async Task<List<T>> GetAll()
        {
            return await _modelRepository.GetAllAsync();
        }

        public async Task<T?> Get(int id)
        {
            return await _modelRepository.GetByIdAsync(id);
        }
    }
}
