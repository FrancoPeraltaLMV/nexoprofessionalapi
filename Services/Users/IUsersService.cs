﻿using NexoProfessional.Models;

namespace NexoProfessional.Services.Users
{
    public interface IUsersService : IModelService<User>
    {
        public Task<User?> GetUserByEmail(User user);
    }
}
