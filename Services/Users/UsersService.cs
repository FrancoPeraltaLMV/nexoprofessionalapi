﻿using NexoProfessional.Models;
using NexoProfessional.Repository.Users;

namespace NexoProfessional.Services.Users
{
    public class UsersService : AModelService<User>, IUsersService
    {
        public UsersService(IUsersRepository modelRepository)
            : base(modelRepository)
        {
        }

        public async Task<User?> GetUserByEmail(User user)
        {
            return await ((IUsersRepository)_modelRepository).GetByEmailAsync(user.Email);
        }

        protected override void SetUpdateData(User oldModel, User newModel)
        {
            oldModel.Name = newModel.Name;
            oldModel.Alias = newModel.Alias;
            oldModel.SurName = newModel.SurName;
            oldModel.Dni = newModel.Dni;
            oldModel.SurName1 = newModel.SurName1;
            oldModel.SurName2 = newModel.SurName2;
            oldModel.Email = newModel.Email;
            oldModel.Password = newModel.Password;
            oldModel.ConfirmPassword = newModel.ConfirmPassword;
            oldModel.Telephone = newModel.Telephone;
            oldModel.CompanyName = newModel.CompanyName;
            oldModel.CIF = newModel.CIF;
            oldModel.CompanyTelephone = newModel.CompanyTelephone;
            oldModel.CompanyEmail = newModel.CompanyEmail;
            oldModel.CountryShort = newModel.CountryShort;
            oldModel.CountryLong = newModel.CountryLong;
            oldModel.Province = newModel.Province;
            oldModel.City = newModel.City;
            oldModel.Availability = newModel.Availability;
            oldModel.Activities = newModel.Activities;
            oldModel.Skills = newModel.Skills;
            oldModel.IsFreelance = newModel.IsFreelance;
            oldModel.Proyect = newModel.Proyect;
            oldModel.Description = newModel.Description;
            oldModel.Tarifa = newModel.Tarifa;
            oldModel.isNexo = newModel.isNexo;
            oldModel.Type = newModel.Type;
            oldModel.imgProfile = newModel.imgProfile;
            oldModel.isAdmin = newModel.isAdmin;
            oldModel.isConfirm = newModel.isConfirm;
        }
    }
}
