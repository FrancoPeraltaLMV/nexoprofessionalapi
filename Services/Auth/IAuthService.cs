﻿using NexoProfessional.Entities.Auth;
using NexoProfessional.Models;

namespace NexoProfessional.Services.Auth
{
    public interface IAuthService
    {
        Task<AuthToken?> SignIn(User user);
    }
}
