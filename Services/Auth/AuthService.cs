﻿using Microsoft.IdentityModel.Tokens;
using NexoProfessional.Entities.Auth;
using NexoProfessional.Models;
using NexoProfessional.Services.Users;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace NexoProfessional.Services.Auth
{
    public class AuthService : IAuthService
    {
        private const int TOKEN_EXPIRATION_TIME_IN_MINUTES = 60;

        private readonly IUsersService _usersService;

        private readonly IConfiguration _configuration;

        public AuthService(IUsersService usersService, IConfiguration configuration)
        {
            _usersService = usersService;
            _configuration = configuration;
        }

        public async Task<AuthToken?> SignIn(User user)
        {
            User? registeredUser = await _usersService.GetUserByEmail(user);

            if (this.areCredentialsValid(user, registeredUser))
            {
                DateTime now = DateTime.UtcNow;

                JwtSecurityToken jwt = new JwtSecurityToken(
                    notBefore: now,
                    expires: now.Add(TimeSpan.FromMinutes(TOKEN_EXPIRATION_TIME_IN_MINUTES)),
                    signingCredentials: new SigningCredentials(
                        new SymmetricSecurityKey(
                            Encoding.ASCII.GetBytes(_configuration.GetValue<string>("JWT_SECRET_KEY"))
                        ),
                        SecurityAlgorithms.HmacSha256
                    )
                );

                return new AuthToken
                {
                    AccessToken = new JwtSecurityTokenHandler().WriteToken(jwt),
                    Expires = TimeSpan.FromMinutes(TOKEN_EXPIRATION_TIME_IN_MINUTES).TotalSeconds
                };
            }

            return null;
        }

        private bool areCredentialsValid(User user, User? registeredUser)
        {
            return
                registeredUser != null &&
                registeredUser.Email == user.Email &&
                registeredUser.Password == user.Password;
        }
    }
}
