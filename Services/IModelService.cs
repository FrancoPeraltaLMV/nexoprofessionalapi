﻿namespace NexoProfessional.Services
{
    public interface IModelService<T> where T : class
    {
        Task<T> Add(T model);

        Task<bool> Update(int id, T model);

        Task<bool> Delete(int id);

        Task<List<T>> GetAll();

        Task<T?> Get(int id);
    }
}
