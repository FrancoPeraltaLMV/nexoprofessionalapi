﻿using NexoProfessional.Models;

namespace NexoProfessional.Services.Email
{
    public interface IEmailSenderService
    {
        Task SendEmailAsync(MailRequest request);
    }
}
