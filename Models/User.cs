﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace NexoProfessional.Models
{
    public class User
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [MaxLength(200)]
        [Column("name")]
        public string Name { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("alias")]
        public string Alias { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("surname")]
        public string SurName { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("dni")]
        public string Dni { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("surname1")]
        public string SurName1 { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("surname2")]
        public string SurName2 { get; set; } = string.Empty;

        [Required]
        [MaxLength(320)]
        [Column("email")]
        public string Email { get; set; } = string.Empty;

        [Required]
        [MaxLength(400)]
        [Column("password")]
        public string Password { get; set; } = string.Empty;

        [Required]
        [MaxLength(400)]
        [Column("confirmpassword")]
        public string ConfirmPassword { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("telephone")]
        public string Telephone { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("companyname")]
        public string CompanyName { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("cif")]
        public string CIF { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("companytelephone")]
        public string CompanyTelephone { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("companyemail")]
        public string CompanyEmail { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("countryshort")]
        public string CountryShort { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("countrylong")]
        public string CountryLong { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("province")]
        public string Province { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("city")]
        public string City { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("availability")]
        public string Availability { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("activities")]
        [NotMapped]
        public string[] Activities { get; set; } = Array.Empty<string>();

        [Required]
        [MaxLength(200)]
        [Column("skills")]
        [NotMapped]
        public string[] Skills { get; set; } = Array.Empty<string>();

        [Required]
        [Column("isfreelance")]
        public bool IsFreelance { get; set; } = false;

        [Required]
        [MaxLength(200)]
        [Column("proyect")]
        public string Proyect { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("description")]
        public string Description { get; set; } = string.Empty;

        [Required]
        [MaxLength(200)]
        [Column("tarifa")]
        public string Tarifa { get; set; } = string.Empty;

        [Required]
        [Column("isnexo")]
        public bool isNexo { get; set; }

        [Required]
        [MaxLength(200)]
        [Column("type")]
        public string Type { get; set; } = string.Empty; // Freelance o Empresa

        [Required]
        [MaxLength(200)]
        [Column("imgprofile")]
        public string imgProfile { get; set; } = string.Empty;

        // Role
        [Column("isAdmin")]
        public bool isAdmin { get; set; } = false;

        // Usuario verificado por el admin
        [Column("isConfirm")]
        public bool isConfirm { get; set; } = false;
    }
}
