﻿using EntityFramework.Exceptions.SqlServer;
using Microsoft.EntityFrameworkCore;
using NexoProfessional.Models;

namespace NexoProfessional
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseExceptionProcessor();
        }

        public DbSet<User> Users { get; set; }
    }
}
