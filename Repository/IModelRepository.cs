﻿namespace NexoProfessional.Repository
{
    public interface IModelRepository<T> where T : class
    {
        public Task<T> CreateAsync(T _object);

        public Task UpdateAsync(T _object);

        public Task<List<T>> GetAllAsync();

        public Task<T?> GetByIdAsync(int id);

        public Task DeleteAsync(int id);
    }
}
