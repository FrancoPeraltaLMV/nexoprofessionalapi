﻿using Microsoft.EntityFrameworkCore;
using NexoProfessional.Models;

namespace NexoProfessional.Repository.Users
{
    public class UsersRepository : AModelRepository<User>, IUsersRepository
    {
        public UsersRepository(ApplicationDbContext context)
            : base(context)
        {
        }
        public async Task<User?> GetByEmailAsync(string email)
        {
            return await _entities.SingleOrDefaultAsync(user => user.Email == email);
        }
    }
}
