﻿using NexoProfessional.Models;

namespace NexoProfessional.Repository.Users
{
    public interface IUsersRepository : IModelRepository<User>
    {
        public Task<User?> GetByEmailAsync(string email);
    }
}
