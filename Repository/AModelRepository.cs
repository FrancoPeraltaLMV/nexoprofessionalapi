﻿using Microsoft.EntityFrameworkCore;

namespace NexoProfessional.Repository
{
    public abstract class AModelRepository<T> : IModelRepository<T> where T : class
    {
        protected readonly DbContext _context;

        protected readonly DbSet<T> _entities;

        public AModelRepository(DbContext context)
        {
            _context = context;
            _entities = context.Set<T>();
        }

        public async Task<T> CreateAsync(T _object)
        {
            var obj = await _entities.AddAsync(_object);

            _context.SaveChanges();

            return obj.Entity;
        }

        public async Task DeleteAsync(int id)
        {
            var obj = await _entities.FindAsync(id);

            if (obj != null)
            {
                _entities.Remove(obj);

                await _context.SaveChangesAsync();
            }
        }

        public async Task<List<T>> GetAllAsync()
        {
            return await _entities.ToListAsync();
        }

        public async Task<T?> GetByIdAsync(int id)
        {
            return await _entities.FindAsync(id);
        }

        public async Task UpdateAsync(T _object)
        {
            _entities.Update(_object);

            await _context.SaveChangesAsync();
        }
    }
}
