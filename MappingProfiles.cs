﻿using AutoMapper;
using NexoProfessional.Dtos;
using NexoProfessional.Models;

namespace NexoProfessional
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<CreateUserClientRequest, User>();
            CreateMap<CreateUserFreelanceRequest, User>();
        }
    }
}
