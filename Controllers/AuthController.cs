﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NexoProfessional.Dtos;
using NexoProfessional.Entities.Auth;
using NexoProfessional.Models;
using NexoProfessional.Services.Auth;

namespace NexoProfessional.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly IAuthService _authService;

        public AuthController(IMapper mapper, IAuthService authService)
        {
            _mapper = mapper;
            _authService = authService;
        }

        [HttpPost("sign-in")]
        public async Task<ActionResult<AuthToken>> SignInUser(SignInUserRequest signInUserRequest)
        {
            User user = _mapper.Map<User>(signInUserRequest);

            AuthToken? authToken = await _authService.SignIn(user);

            if (authToken != null)
            {
                return authToken;
            }

            return Unauthorized();
        }
    }
}
