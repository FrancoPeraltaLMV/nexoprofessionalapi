﻿using AutoMapper;
using EntityFramework.Exceptions.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NexoProfessional.Dtos;
using NexoProfessional.Helpers;
using NexoProfessional.Models;
using NexoProfessional.Services.Email;
using NexoProfessional.Services.Users;

namespace NexoProfessional.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly IUsersService _userService;

        private readonly IEmailSenderService _emailSenderService;

        private readonly ApplicationDbContext _context;

        public UsersController(IMapper mapper, IUsersService userService, IEmailSenderService emailSenderService, ApplicationDbContext context)
        {
            _mapper = mapper;
            _userService = userService;
            _emailSenderService = emailSenderService;
            _context = context;
        }

        // GET: api/user
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            return await _userService.GetAll();
        }

        // GET: api/user/Pending
        [HttpGet("Pending")]
        public async Task<ActionResult<IEnumerable<User>>> GetUsersPending()
        {
            var pending = await _context.Users.Where(x => x.isConfirm == false).ToListAsync();

            if (pending == null)
            {
                return NotFound();
            }

            return pending;
        }

        // GET: api/user/Approved
        [HttpGet("Approved")]
        public async Task<ActionResult<IEnumerable<User>>> GetUsersApproved()
        {
            var approved = await _context.Users.Where(x => x.isConfirm == true).ToListAsync();

            if (approved == null)
            {
                return NotFound();
            }

            return approved;
        }

        // GET: api/user/Activities
        [HttpGet("Activities")]
        public async Task<ActionResult<IEnumerable<User>>> GetUsersActivities()
        {
            var activities = await _context.Users.Where(x => x.Activities.Any()).ToListAsync();

            if (activities == null)
            {
                return NotFound();
            }

            return activities;
        }

        // GET: api/user/Skils
        [HttpGet("Skils")]
        public async Task<ActionResult<IEnumerable<User>>> GetUsersSkils()
        {
            var skils = await _context.Users.Where(x => x.Skills.Any()).ToListAsync();

            if (skils == null)
            {
                return NotFound();
            }

            return skils;
        }


        // GET: api/user/Freelance
        [HttpGet("Freelance")]
        public async Task<ActionResult<IEnumerable<User>>> GetUsersFreelance()
        {
            var freelance = await _context.Users.Where(x => x.Type == "Freelance").ToListAsync();

            if (freelance == null)
            {
                return NotFound();
            }

            return freelance;
        }

        // GET: api/user/Client
        [HttpGet("Client")]
        public async Task<ActionResult<IEnumerable<User>>> GetUsersClient()
        {
            var client = await _context.Users.Where(x => x.Type == "Client").ToListAsync();

            if (client == null)
            {
                return NotFound();
            }

            return client;
        }

        // GET: api/user/Location
        [HttpGet("Location/{location}")]
        public async Task<ActionResult<IEnumerable<User>>> GetUsersLocation(string location)
        {
            var location_list = await _context.Users.Where(x => x.CountryShort == location || 
                                                         x.CountryLong == location ||
                                                         x.Province == location ||
                                                         x.City == location).ToListAsync();

            if (location_list == null)
            {
                return NotFound();
            }

            return location_list;
        }

        // GET: api/user/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(int id)
        {
            var user = await _userService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // PUT: api/user/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(int id, User user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            if (await _userService.Get(id) == null)
            {
                return NotFound();
            }

            try
            {
                await _userService.Update(id, user);
            }
            catch (UniqueConstraintException)
            {
                return UnprocessableEntity();
            }

            return NoContent();
        }

        // PUT: api/user/ConfirmApproved/5
        [HttpPut("ConfirmApproved/{id}")]
        public async Task<IActionResult> PutUserApproved(int id, User user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            if (await _userService.Get(id) == null)
            {
                return NotFound();
            }

            if(!user.isAdmin)
            {
                return BadRequest();
            }

            try
            {
                user.isConfirm = true;

                await _context.SaveChangesAsync();

                //MailRequest mailApproved = new MailRequest();

                //mailApproved.Email = user.Email;
                //mailApproved.Subject = "Nexo Professional - Cuenta aprobada";
                //mailApproved.Body = "Su cuenta ha sido aceptada";

                //await _emailSenderService.SendEmailAsync(mailApproved);
            }
            catch (UniqueConstraintException)
            {
                return UnprocessableEntity();
            }

            return NoContent();
        }

        // POST: api/user/ResetPassword
        [HttpPost("ResetPassword")]
        public async Task<IActionResult> PutResetPassword(string email, string password, string confirmPassword)
        {
            var user = await _context.Users.Where(x => x.Email == email).FirstOrDefaultAsync();
            if (user == null)
            {
                return NotFound();
            }

            if(password != confirmPassword)
            {
                return BadRequest("Las contraseñas no son iguales");
            }

            user.Password = password;
            user.ConfirmPassword = confirmPassword;

            _context.Update(user);
            await _context.SaveChangesAsync();
            return Ok();
        }

        // POST: api/User/Freelance
        [HttpPost("Freelance")]
        public async Task<ActionResult<User>> PostUserFreelance(CreateUserFreelanceRequest userFreelanceDto)
        {
            User userFreelance = _mapper.Map<User>(userFreelanceDto);

            try
            {
                await _userService.Add(userFreelance);

                //MailRequest mail = new MailRequest();

                //mail.Email = userFreelanceDto.Email;
                //mail.Subject = "Nexo Professional - Confirmar Registro";
                //mail.Body = "Su cuenta esta siendo procesada, una vez confirmada le llegara un correo avisando que su cuenta fue aceptado.";

                //await _emailSenderService.SendEmailAsync(mail);

                //MailRequest mailNexo = new MailRequest();

                //mailNexo.Email = "info@nexohiring.com";
                //mailNexo.Subject = "Nexo Professional - Nuevo Freelance";
                //mailNexo.Body = $"Un nuevo usuario freelance se ha registrado: {userFreelanceDto.Email} {userFreelanceDto.Name} {userFreelanceDto.Dni} {userFreelanceDto.Telephone}";

                //await _emailSenderService.SendEmailAsync(mailNexo);
            }
            catch (UniqueConstraintException)
            {
                return UnprocessableEntity();
            }


            return CreatedAtAction(nameof(GetUser), new { id = userFreelance.Id }, userFreelance);
        }

        // POST: api/User/Client
        [HttpPost("Client")]
        public async Task<ActionResult<User>> PostUserClient(CreateUserClientRequest userClientDto)
        {
            userClientDto.Password = Encrypt.GetSHA256(userClientDto.Password);
            userClientDto.ConfirmPassword = Encrypt.GetSHA256(userClientDto.ConfirmPassword);

            User userClient = _mapper.Map<User>(userClientDto);

            try
            {
                await _userService.Add(userClient);

                //MailRequest mail = new MailRequest();

                //mail.Email = userClientDto.Email;
                //mail.Subject = "Nexo Professional - Confirmar Registro";
                //mail.Body = "Su cuenta esta siendo procesada, una vez confirmada le llegara un correo avisando que su cuenta fue aceptado.";

                //await _emailSenderService.SendEmailAsync(mail);

                //MailRequest mailNexo = new MailRequest();

                //mailNexo.Email = "info@nexohiring.com";
                //mailNexo.Subject = "Nexo Professional - Nuevo Cliente/Empresa";
                //mailNexo.Body = $"Un nuevo usuario cliente se ha registrado: {userClientDto.Email} {userClientDto.Name} {userClientDto.CIF} {userClientDto.Telephone}";

                //await _emailSenderService.SendEmailAsync(mailNexo);
            }
            catch (UniqueConstraintException)
            {
                return UnprocessableEntity();
            }


            return CreatedAtAction(nameof(GetUser), new { id = userClient.Id }, userClient);
        }

        // DELETE: api/user/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var user = await _userService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            if(!user.isAdmin)
            {
                return BadRequest();
            }

            await _userService.Delete(id);

            return NoContent();
        }
    }
}
